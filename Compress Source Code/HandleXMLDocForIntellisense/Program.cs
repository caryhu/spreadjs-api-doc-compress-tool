﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using YamlDotNet.Serialization;

namespace HandleXMLDocForIntellisense
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"[{DateTime.UtcNow}]:Start");
            var yamlFileList = Directory.GetFiles(@".\input\yaml", @"*.yml");
            var xmlFileList = Directory.GetFiles(@".\input\xml", @"*.xml");
            var dataDir = new Dictionary<string, int>();
            foreach (var ymlFile in yamlFileList)
            {
                Console.WriteLine($"[{DateTime.UtcNow}]:Start Get YAML: {ymlFile}");

                using (var reader = new StreamReader(ymlFile))
                {
                    var yamlObject = new Deserializer().Deserialize(reader);
                    var data = new YamlQuery(yamlObject)
                               .On("items")
                               .Get("commentId")
                               .ToList<string>();
                    foreach (var item in data)
                    {
                        if (!dataDir.ContainsKey(item))
                        {
                            dataDir.Add(item, 1);
                        }
                    }
                }
            }
            foreach (var item in xmlFileList)
            {
                var xml = LoadXMLFile(item);
                Console.WriteLine($"[{DateTime.UtcNow}]:Start Handle {item}");
                HandleContent(xml, dataDir);
                SaveXMLContent(xml, item);
            }
            Console.WriteLine($"[{DateTime.UtcNow}]:End. Output dir: .\\input\\compressed");
        }
        static XElement LoadXMLFile(string fileName)
        {
            return XElement.Load(fileName);
        }
        static XElement HandleContent(XElement content, Dictionary<string, int> cachedAssembly)
        {
            var memberList = content.Element("members").Elements();
            var waitingForDelete = new List<XElement>() { };
            foreach (var member in memberList)
            {
                var memberName = member.Attribute("name").Value;
                if (!cachedAssembly.ContainsKey(memberName))
                {
                    waitingForDelete.Add(member);
                }
            }
            foreach (var item in waitingForDelete)
            {
                item.Remove();
            }
            return content;
        }
        static void SaveXMLContent(XElement content, string originFileName)
        {
            if (!Directory.Exists(@".\input\compressed\"))
            {
                Directory.CreateDirectory(@".\input\compressed\");
            }
            var file = Path.GetFileName(originFileName);
            string compressedPath = @$".\input\compressed\{file}";
            content.Save(compressedPath);
        }
    }
}
